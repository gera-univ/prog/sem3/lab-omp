#include <iostream>
#include <iomanip>
#include <cmath>
#include <functional>
#include <cstring>

void print_usage(char *argv0) {
    std::cout << "Usage: " << argv0 << " symmetric|newton N\n";
}

double symmetric(const std::function<double(double)> &f, double x, double delta) {
    return (f(x + delta) - f(x - delta)) / (2.0 * delta);
}

double newton(const std::function<double(double)> &f, double x, double delta) {
    return (f(x + delta) - f(x)) / (delta);
}

int main(int argc, char **argv) {
    if (argc != 3) {
        print_usage(argv[0]);
        exit(1);
    }

    auto quotient = symmetric;
    if (strcmp("newton", argv[1]) == 0) {
        quotient = newton;
    } else if (strcmp("symmetric", argv[1]) != 0) {
        print_usage(argv[0]);
        exit(1);
    }

    int N;
    std::istringstream(argv[2]) >> N;

    double a = 0.0;
    double b = 2.0;
    auto f = [&a, &b](double x) {
        if (x < a)
            x = a;
        else if (x > b)
            x = b;

        if (x < M_PI_2) {
            return cos(x);
        }
        return M_PI_2 - x;
    };

    double delta = (a + b) / (N + 1.0);

    const double expected = 0.0;

    int i;
    double maximum = std::numeric_limits<double>::lowest();
#pragma omp parallel default(none) shared(N, a, delta, f, quotient, maximum) private(i)
    {
#pragma omp for reduction(max: maximum)
        for (i = 0; i < N + 1; ++i) {
            maximum = std::max(quotient(f, a + i * delta, delta), maximum);
        }
    }

    std::cout << std::setprecision(18) << "result\t\t\t" << maximum << "\nexpected\t\t" << expected
              << "\ndifference\t\t" << maximum - expected
              << "\n";

    return 0;
}
