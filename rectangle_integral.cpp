#include <iostream>
#include <iomanip>
#include <sstream>

void print_usage(char *argv0) {
    std::cout << "Usage: " << argv0 << " N\n";
}

int main(int argc, char **argv) {
    if (argc != 2) {
        print_usage(argv[0]);
        exit(1);
    }

    int N;
    std::istringstream(argv[1]) >> N;

    double a = 0.0;
    double b = 1.0;
    auto f = [](double x) {
        return x*x - 2.0*x;
    };

    double delta = (a + b) / (N + 1.0);

    const double expected = -2.0/3.0;

    int i;
    double x, sum = 0;
#pragma omp parallel default(none) shared(N, a, b, delta, f, sum) private(i, x)
    {
#pragma omp for reduction(+: sum)
        for (i = 0; i < N + 1; ++i) {
            x = a + i * delta;
            if (x > b)
                x = b - delta;
            sum += f(x) * delta;
        }
    }

    std::cout << std::setprecision(18) << "result\t\t\t" << sum << "\nexpected\t\t" << expected << "\ndifference\t\t" << sum - expected
              << "\n";

    return 0;
}
